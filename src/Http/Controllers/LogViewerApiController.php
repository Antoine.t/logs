<?php namespace Arcanedev\LogViewer\Http\Controllers;

use Arcanedev\LogViewer\Contracts\LogViewer as LogViewerContract;
use Arcanedev\LogViewer\Entities\LogEntry;
use Arcanedev\LogViewer\Exceptions\LogNotFoundException;
use Arcanedev\LogViewer\Tables\StatsTable;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * Class     LogViewerController
 *
 * @package  LogViewer\Http\Controllers
 * @author   ARCANEDEV <arcanedev.maroc@gmail.com>
 */
class LogViewerApiController extends Controller
{
    /* -----------------------------------------------------------------
     |  Properties
     | -----------------------------------------------------------------
     */

    /**
     * The log viewer instance
     *
     * @var \Arcanedev\LogViewer\Contracts\LogViewer
     */
    protected $logViewer;

    /* -----------------------------------------------------------------
     |  Constructor
     | -----------------------------------------------------------------
     */

    /**
     * LogViewerController constructor.
     *
     * @param  \Arcanedev\LogViewer\Contracts\LogViewer  $logViewer
     */
    public function __construct(LogViewerContract $logViewer)
    {
        $this->logViewer = $logViewer;
    }

    /* -----------------------------------------------------------------
     |  Main Methods
     | -----------------------------------------------------------------
     */

    /**
     * List all logs.
     *
     * @return Response
     */
    public function listLogs()
    {
        return response()->json($this->logViewer->statsTable()->rows(), 200);
    }

    /**
     * Show the log.
     *
     * @param  string $date
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($date)
    {
        return response()->json($this->getLogOrFail($date), 200);
    }

    /**
     * Filter the log entries by level.
     *
     * @param  string                    $date
     * @param  string                    $level
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function showByLevel($date, $level)
    {
        return response()->json($this->logViewer->entries($date, $level), 200);
    }

    /**
     * Delete a log.
     *
     * @param  string $date
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($date)
    {
        $this->logViewer->delete($date);

        return response()->json(null, 204);
    }

    /**
     * Get a log or fail
     *
     * @param  string  $date
     *
     * @return \Arcanedev\LogViewer\Entities\Log|null
     */
    protected function getLogOrFail($date)
    {
        $log = null;

        try {
            $log = $this->logViewer->get($date);
        } catch (LogNotFoundException $e) {
            abort(404, $e->getMessage());
        }

        return $log;
    }
}
